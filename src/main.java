/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/14/2017
 */

import Audio.Music;
import Game.CreditsMenu;
import Game.Game;
import Game.MainMenu;
import Game.FlowerBed;
import Game.HowToPlayMenu;
import javafx.application.Application;
import javafx.event.EventHandler;
import Game.StateSelectedEvent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * @author nowace
 * @version 2017.12.14
 */
public class main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //load in options here
        Music music = new Music("src/Assets/Music/happy.mp3");
        music.setVolume(0.1);

        MainMenu menu = new MainMenu();
        Scene mainMenuScene = new Scene(menu, 800, 600);

        CreditsMenu creditsMenu = new CreditsMenu();
        Scene creditsMenuScene = new Scene(creditsMenu,800,600);

        HowToPlayMenu howToPlayMenu = new HowToPlayMenu();
        Scene howToPlayScene = new Scene(howToPlayMenu);

        StackPane root = new StackPane();
        Canvas canvas = new Canvas(800,600);
        root.getChildren().add(canvas);
        Game game = new FlowerBed(root,canvas);
        primaryStage.setScene(mainMenuScene);
        primaryStage.show();

        menu.setOnStateChanged(event->{
            switch (event.getStateName()){
                case "game":
                    primaryStage.setScene(game);
                    game.start();
                    music.start();
                    break;
                case "howTo":
                    primaryStage.setScene(howToPlayScene);
                    break;
                case "credits":
                    primaryStage.setScene(creditsMenuScene);
                    break;
                case "exit":
                    System.exit(0);
                    break;
                default:
                    break;
            }
        });

        EventHandler<StateSelectedEvent> doneEvent = event -> {
            if(event.getStateName().equals("done")){
                primaryStage.setScene(mainMenuScene);
            }
        };

        creditsMenu.setOnStateChanged(doneEvent);
        howToPlayMenu.setOnStateChanged(doneEvent);
        game.setOnStateChanged(event -> {
            if(event.getStateName().equals("done")){
                primaryStage.setScene(mainMenuScene);
                game.stop();
                music.stop();
            }
        });

    }
}
