/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/9/2018
 */
package Graphics;

import Entities.Entity;
import javafx.scene.canvas.GraphicsContext;


/**
 * @author nowace
 * @version 2018.01.09
 */
public abstract class Sprite extends Entity {
    protected Texture texture;

    @Override
    public void draw(GraphicsContext context) {
        super.draw(context);
        context.drawImage(texture.getImage(),0 - this.origin.getX(),0 - this.origin.getY());
        context.restore();
    }


    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
