/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/5/2018
 */
package Graphics;

import javafx.scene.canvas.GraphicsContext;

/**
 * @author nowace
 * @version 2018.01.05
 */
public class Background extends Texture{
    public Background(String filepath) {
        super(filepath);
    }

    public void draw(GraphicsContext context){
        context.save();
        context.translate(0,0);
        context.drawImage(this.getImage(),0,0);
        context.restore();
    }
}
