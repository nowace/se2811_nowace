/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/8/2018
 */
package Entities.Bees;

import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;

import java.util.List;
import Math.Vector;

/**
 * Defines a bee that flys in a circle
 * @author nowace
 * @version 2018.01.08
 */
public class CircleBee extends Bee {

    private double currentTime;
    private double radius;
    private Vector originPosition;

    /**
     * Constructs a Circle Bee with the given position rotation speed and radius
     * @param position position of the circle origin
     * @param rotSpeed rotation speed of the bee
     * @param radius radius of the circle
     */
    public CircleBee(Vector position, double rotSpeed, double radius, double speed) {
        super( new Texture("Assets/Bees/circle_bee.png"), position, rotSpeed);
        this.energy = 2000;
        this.currentTime = 0;
        this.radius = radius;
        this.originPosition = position;
        this.speed = speed;
    }

    /**
     * Ticks the Circle bee once. Calculates the velocity
     * to move the bee in a circle
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        currentTime += timestep;
        //double changedRadius = Math.abs(radius * Math.sin(currentTime));
        double x = this.originPosition.getX() + Math.cos(currentTime) * radius;
        double y = this.originPosition.getY() + Math.sin(currentTime) * radius;
        Vector vel = new Vector(x - this.position.getX(), y - this.getPosition().getY());
        //vel = vel.scalar(speed);
        this.setVelocity(vel.scalar(1/timestep));
        this.setRotation(this.rotation + speed);
        this.bound.setPosition(this.position);
    }

    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
    }
}