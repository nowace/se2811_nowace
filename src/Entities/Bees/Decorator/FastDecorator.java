package Entities.Bees.Decorator;

import Entities.Bees.Bee;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Describes a decorator that makes a bee go faster
 * Created by nowace
 * @version 2018.1.6
 */
public class FastDecorator extends BeeDecorator {

    private double speedFactor;

    /**
     * Constructs the decorator with the given child and speed factor
     * @param bee child bee
     * @param speedFactor speed factor
     */
    public FastDecorator(Bee bee, double speedFactor) {
        super(bee);
        this.speedFactor = speedFactor;
    }

    /**
     * Draws the child bee and a yellow rectangle
     * @param context
     */
    @Override
    public void draw(GraphicsContext context) {
        super.draw(context);
        context.save();
        context.translate(this.getBase().getPosition().getX(),this.getBase().getPosition().getY());
        context.rotate(this.getBase().getRotation());
        context.setStroke(Color.YELLOW);
        context.strokeRect(0-this.getBase().getOrigin().getX(),0 - this.getBase().getOrigin().getY(),this.getBase().getBound().getSize().getX()+1,this.getBase().getBound().getSize().getY()+1);
        context.restore();
    }

    /**
     * increases the timestep of the child bee
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep * speedFactor);
    }
}
