package Entities.Bees.Decorator;

import Entities.Bees.Bee;
import Entities.Effects.Effect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.BoundingGeometry.Bound;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

/**
 * Describes a standard Bee Decorator
 * Created by nowace
 * @version 2018.1.6
 */
public abstract class BeeDecorator extends Bee {

    protected Bee child;

    /**
     * Constructs the bee decorator
     * @param bee
     */
    public BeeDecorator(Bee bee) {
        super(bee.getTexture(), bee.getPosition(), bee.getSpeed());
        this.self = bee.getPointer();
        this.self.setEntity(this);
        this.child = bee;
    }

    /**
     * Draws the child bee
     * @param context
     */
    @Override
    public void draw(GraphicsContext context) {
        this.child.draw(context);
    }

    /**
     * Ticks the child bee
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        this.child.tick(timestep);
        if(this.child.isDead()){
            this.dead = true;
        }
    }

    /**
     * Draws the debug of the child bee
     * @param context
     */
    @Override
    public void drawDebug(GraphicsContext context) {
        this.child.drawDebug(context);
        this.addDebugString(this.toString());
    }

    /**
     * Gets the child bee
     * @return child bee
     */
    public Bee getBee() {
        return child;
    }

    public Bee remove(BeeDecorator parent){
        if(parent != null){
            parent.child = this.child;
        }

        this.child = null;
        return this;
    }

    /**
     * child bee effects other entity
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        this.child.effect(other);
    }

    /**
     * Adds effect to child bee
     * @param effect
     */
    @Override
    public void addEffect(Effect effect) {
        this.child.addEffect(effect);
    }

    @Override
    public void setSpeed(double speed) {
        this.child.setSpeed(speed);
    }

    @Override
    public double getSpeed() {
        return this.child.getSpeed();
    }

    @Override
    public double getTimestepScalar() {
        return this.child.getTimestepScalar();
    }

    @Override
    public void setTimestepScalar(double timestepScalar) {
        this.child.setTimestepScalar(timestepScalar);
    }

    @Override
    public int getEnergy() {
        return this.child.getEnergy();
    }

    @Override
    public void setEnergy(int energy) {
        this.child.setEnergy(energy);
    }

    @Override
    public List<Effect> getEffects() {
        return this.child.getEffects();
    }

    @Override
    public Bound getBound() {
        return this.child.getBound();
    }

    /**
     * Adds a string to the debug display of child bee
     * @param string debug string to add
     */
    @Override
    public void addDebugString(String string) {
        this.child.addDebugString(string);
    }

    /**
     * Checks the collisions of the child bee
     * @param entities list of entities to check collisions with
     * @return
     */
    @Override
    public EntityPointer checkCollisions(List<EntityPointer> entities) {
        return this.child.checkCollisions(entities);
    }
}
