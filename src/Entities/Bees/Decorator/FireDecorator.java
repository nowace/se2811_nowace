/*
 * se2811_nowace
 * Eric Nowac
 * Created 1/17/2018
 */
package Entities.Bees.Decorator;

import Entities.Bees.Bee;
import Entities.Effects.DamageEffect;
import Entities.EntityPointer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


/**
 * Describes a fire decorator that burns surrounding bees
 * @author nowace
 * @version 2018.01.17
 */
public class FireDecorator extends BeeDecorator {

    private int damage;
    private int tickTime;

    /**
     * Constructs a firedecorator with the given child bee and parameters for the damage effect it applies
     * @param bee child be
     * @param damage damage of the damage effect
     * @param tickTime tick time of the damage effect
     */
    public FireDecorator(Bee bee, int damage, int tickTime) {
        super(bee);
        this.damage = damage;
        this.tickTime = tickTime;
    }

    /**
     * Draws the child bee and a red square
     * @param context
     */
    @Override
    public void draw(GraphicsContext context) {
        super.draw(context);
        context.save();
        context.translate(this.getBase().getPosition().getX(),this.getBase().getPosition().getY());
        context.rotate(this.getBase().getRotation());
        context.setStroke(Color.RED);
        context.strokeRect(0-this.getBase().getOrigin().getX(),0 - this.getBase().getOrigin().getY(),this.getBase().getBound().getSize().getX() + 2,this.getBase().getBound().getSize().getY() + 2);
        context.restore();
    }

    /**
     * Ticks the child bee
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
    }

    /**
     * Applies damage effect to other entity
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        other.getEntity().addEffect(new DamageEffect(damage,tickTime));
    }
}
