package Entities.Bees.Decorator;

import Entities.Bees.Bee;
import Entities.Effects.Effect;
import Entities.Effects.InvincibleEffect;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Describes a shield decorator that protects the child bee
 * Created by nowace
 * @version 2018.1.6
 */
public class ShieldDecorator extends BeeDecorator {

    /**
     * Creates a shield decorator with the given child bee
     * @param bee
     */
    public ShieldDecorator(Bee bee) {
        super(bee);
        this.setInvincible(true);
    }

    /**
     * Draws the child and a blue circle
     * @param context
     */
    @Override
    public void draw(GraphicsContext context) {
        super.draw(context);
        context.save();
        context.translate(this.getBase().getPosition().getX(),this.getBase().getPosition().getY());
        context.rotate(this.getBase().getRotation());
        context.setStroke(Color.BLUE);
        context.strokeOval(0-this.getBase().getOrigin().getX(),0 - this.getBase().getOrigin().getY(),this.getBase().getBound().getSize().getX() + 3,this.getBase().getBound().getSize().getY() + 3);
        context.restore();
    }

    /**
     * Ticks the child bee
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
    }

    /**
     * Adds a temporary invincibility effect to prevent flowers effecting the bee right away
     * @param effect ignores this
     */
    @Override
    public void addEffect(Effect effect) {
        super.addEffect(new InvincibleEffect(20));
        System.out.println("no effectS");
    }
}
