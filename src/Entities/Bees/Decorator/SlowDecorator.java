package Entities.Bees.Decorator;

import Entities.Bees.Bee;
import Entities.Effects.Effect;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Describes a slow decorator that slows the child bee
 * Created by nowace
 * @version 2018.1.6
 */
public class SlowDecorator extends BeeDecorator {

    private double slowDownFactor;

    /**
     * Constructs a slow decorator with the given child and slowdown factor
     * @param bee child
     * @param slowDownFactor slowdown factor
     */
    public SlowDecorator(Bee bee, double slowDownFactor) {
        super(bee);
        this.slowDownFactor = slowDownFactor;
    }

    /**
     * Draws the child bee and a green rectangle
     * @param context
     */
    @Override
    public void draw(GraphicsContext context) {
        super.draw(context);
        context.save();
        context.translate(this.getBase().getPosition().getX(),this.getBase().getPosition().getY());
        context.rotate(this.getBase().getRotation());
        context.setStroke(Color.GREEN);
        context.strokeRect(0-this.getBase().getOrigin().getX(),0 - this.getBase().getOrigin().getY(),this.getBase().getBound().getSize().getX() + 4,this.getBase().getBound().getSize().getY() + 4);
        context.restore();
    }

    /**
     * Ticks the child at a reduced timestep
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep * this.slowDownFactor);
    }
}
