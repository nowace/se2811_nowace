package Entities.Bees;

import Entities.Effects.InvincibleEffect;
import Entities.Effects.StunEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import javafx.scene.canvas.GraphicsContext;

import java.util.List;

/**
 * Created by boulgerb on 12/19/2017.
 */
public class ZigZagBee extends Bee {
    private static final double VERTICAL_SPEED_MULTIPLIER = 2;

    private Vector screenSize;
    private boolean goingRight;

    /**
     * Constructs a ZigZagBee with the given position
     * and screensize
     * @param position starting position of the bee
     * @param screenSize size of the screen
     */
    public ZigZagBee(Vector position, Vector screenSize) {
        super(new Texture("Assets/Bees/zig_zag_bee.png"), position, 400);
        this.screenSize = screenSize;
        this.energy = 2000;
        goingRight = true;
        this.setVelocity(new Vector(0,speed));
    }

    /**
     * Ticks the bee forward one timestep. Calculates
     * the velocity needed to follow the zigzag pattern
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);


        double velY = this.getVelocity().getY();
        double velX = 0;

        if(this.getPosition().getX() > screenSize.getX()){
            goingRight = false;
        }

        if(this.getPosition().getX() < 0){
            goingRight = false;
        }

        if(this.getPosition().getY() + (velY * timestep) > screenSize.getY() || this.getPosition().getY() + (velY * timestep) < 0){
            velY *= -1;
            velX+=speed * VERTICAL_SPEED_MULTIPLIER * ((goingRight)?1:-1);
        }
        this.setVelocity(new Vector(velX,velY));
        this.bound.setPosition(this.position);
    }

    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
    }
}
