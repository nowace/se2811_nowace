package Entities.Bees;

import Entities.Bees.Decorator.BeeDecorator;
import Entities.Effects.InvincibleEffect;
import Entities.Effects.StunEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Sprite;
import Graphics.Texture;
import Math.BoundingGeometry.BoundingCircle;
import Math.Vector;

import java.util.LinkedList;
import java.util.List;

/**
 * A class that describes an abstract bee
 * Created by boulgerb on 12/19/2017.
 * Edited by nowace
 * @version 2017.19.12
 */
public abstract class Bee extends Sprite{
    protected static final int DEBUG_ENERGY_LEVEL = -1000;
    private static final int STUN_DURATION = 4;
    private static final int INVINCIBILITY_DURATION = 10;
    private static final int BOUNDS_RADIUS_OFFSET = 10;

    protected double speed;
    protected double timestepScalar;

    /**
     * Constructs a bee with a given texture position and speed
     * @param texture texture for the bee
     * @param position position of the bee
     * @param speed speed of the bee
     */
    public Bee(Texture texture, Vector position, double speed) {
        this.effects = new LinkedList<>();
        this.position = position;
        this.velocity = new Vector(0,0);
        this.rotation = 0;
        this.dead = false;
        this.texture = texture;
        this.speed = speed;
        this.origin = new Vector(this.texture.getImage().getWidth()/2,this.texture.getImage().getHeight()/2);
        bound = new BoundingCircle(position,
                ((texture.getImage().getWidth()>texture.getImage().getHeight())?
                        texture.getImage().getWidth()/2:
                        texture.getImage().getHeight()/2)-BOUNDS_RADIUS_OFFSET);
        timestepScalar = 1;
    }

    /**
     * Ticks the bee with the given timestep
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep * timestepScalar);
        if(energy <= 0 && energy != DEBUG_ENERGY_LEVEL){
            this.dead = true;
        }
        if(energy != DEBUG_ENERGY_LEVEL){
            this.energy--;
        }
    }

    /**
     * Describes how the bee effects other entities
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        //other.getEntity().addEffect(new StunEffect(STUN_DURATION,other.getEntity().getVelocity()));
        //other.getEntity().addEffect(new InvincibleEffect(INVINCIBILITY_DURATION));
    }

    public void setSpeed(double speed){
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }

    public double getTimestepScalar() {
        return timestepScalar;
    }

    public void setTimestepScalar(double timestepScalar) {
        this.timestepScalar = timestepScalar;
    }

    public Bee getBase(){
        if(! (this instanceof BeeDecorator)) return this;
        return ((BeeDecorator)this).getBee().getBase();
    }

    /**
     * Removes a decorator with the given class. HAS TO BE RUN ON THE TOP MOST BEE
     * @param decorator Class of decorator to remove
     * @return
     */
    public Bee removeDecorator(Class decorator){
        return this.removeDecorator(decorator, null);
    }

    /**
     * Removes a decorator with the given class and parent
     * @param decorator Class of decorator to remove
     * @param parent parent of the current decorator
     * @return
     */
    private Bee removeDecorator(Class decorator, BeeDecorator parent){
        if(!(this instanceof BeeDecorator)) return this;
        if(this.getClass().getName().equals(decorator.getName())){
            Bee temp = ((BeeDecorator)this).getBee();
            ((BeeDecorator) this).remove(parent);
            return temp;
        }else{
            ((BeeDecorator)this).getBee().removeDecorator(decorator, (BeeDecorator) this);
            return this;
        }
    }

    /**
     * Checks collisions with all the provided entites
     * @param entities list of entities to check collisions with
     * @return true if a collision happened
     */
    @Override
    public EntityPointer checkCollisions(List<EntityPointer> entities) {
        EntityPointer toReturn = null;
        EntityPointer pointer = this.getPointer();
        if(this.getBound() != null){
            for(EntityPointer e : entities){
                if(e.getEntity().getBound() != null){
                    if(pointer.getEntity().getBound().intersects(e.getEntity().getBound()) && !e.getEntity().equals(pointer.getEntity())){
                        e.getEntity().effect(pointer);
                        toReturn = e;
                    }
                }
            }

        }
        return toReturn;
    }
}
