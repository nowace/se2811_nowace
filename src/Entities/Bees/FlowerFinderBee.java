package Entities.Bees;

import Entities.Bees.Decorator.BeeDecorator;
import Entities.Entity;
import Entities.EntityPointer;
import Entities.Flowers.Flower;
import Graphics.Texture;
import Math.Vector;
import Math.Util;
import java.util.List;

/**
 * Defines a bee that goes through a list of flowers and randomly picks one to fly towards
 * Created by boulgerb on 12/19/2017.
 * @author nowace
 * @version 2017.12.19
 */
public class FlowerFinderBee extends Bee{
    private List<EntityPointer> flowers;
    private int flowerIndex;

    /**
     * Constructs a bee with the given position and flower targets
     * @param position
     * @param flowers
     */
    public FlowerFinderBee(Vector position, List<EntityPointer> flowers) {
        super(new Texture("Assets/Bees/flower_finder.png"), position, 100);
        this.flowers = flowers;
        flowerIndex = Util.RandomInt(0,flowers.size());
        this.energy = 10000;
    }

    /**
     * Ticks the bee forward by one timestep.
     * calculates the velocity to make the
     * bee move towards the selected flower
     * @param timestep timestep of the tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        Flower flower = (Flower)flowers.get(flowerIndex).getEntity();
        double distance = this.position.distanceSQRT(flower.getPosition());
        double factor = speed/distance;
        double x = flower.getPosition().getX() - this.getPosition().getX();
        double y = flower.getPosition().getY() - this.getPosition().getY();
        Vector vel = new Vector(x * factor, y * factor);
        this.setVelocity(vel);
        this.bound.setPosition(this.position);
    }

    /**
     * Checks collision with list of entities
     * @param entities entities to check
     * @return true if collision occurred
     */
    @Override
    public EntityPointer checkCollisions(List<EntityPointer> entities) {
        EntityPointer toReturn = null;
        EntityPointer pointer = this.getPointer();
        for(EntityPointer e : entities){
            if(pointer.getEntity().getBound().intersects(e.getEntity().getBound()) && !e.getEntity().equals(pointer.getEntity())){
                e.getEntity().effect(pointer);
                toReturn = e;
                if(e.equals(flowers.get(flowerIndex))){ //Check to see if you hit the selected flower
                    int oldIndex = flowerIndex;
                    while(flowerIndex == oldIndex){
                        flowerIndex = Util.RandomInt(0,flowers.size());//find a new flower
                    }
                }
            }
        }
        return toReturn;
    }

    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
    }
}
