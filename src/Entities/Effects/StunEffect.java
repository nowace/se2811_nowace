/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/9/2018
 */
package Entities.Effects;

import Entities.Entity;
import Entities.EntityPointer;
import Math.Vector;

/**
 * Defines an Effect that stuns the effected entity
 * @author nowace
 * @version 2018.01.09
 */
public class StunEffect extends Effect {

    private Vector previousVelocity;

    /**
     * Constructs a stun effect with the given duration and the velocity
     * of the entity before it was effected
     * @param duration duration of the effect
     * @param prevVel velocity before it was effected
     */
    public StunEffect(int duration, Vector prevVel){
        this.name = "Stun";
        this.isDone = false;
        this.tickTime = duration;
        this.ticksLeft = duration;
        this.previousVelocity = prevVel;
    }

    /**
     * Effects the given entity and ticks down the ticks left
     * @param entity entity to effect
     */
    @Override
    public void effect(EntityPointer entity) {
        if(ticksLeft > 0){
            entity.getEntity().setVelocity(new Vector(0,0));
            ticksLeft--;
        }else{
            this.isDone = true;
            entity.getEntity().setVelocity(previousVelocity);
        }
    }
}
