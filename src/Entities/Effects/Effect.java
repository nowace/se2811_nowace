package Entities.Effects;

import Entities.Entity;
import Entities.EntityPointer;

/**
 * Created by boulgerb on 12/19/2017.
 */
public abstract class Effect{
    protected int tickTime;
    protected int ticksLeft;
    protected boolean isDone;
    protected String name;


    public void effect(EntityPointer entity){
        if(entity.getEntity().isInvincible()){
            return;
        }
    }

    public boolean isDone() {
        return isDone;
    }

    public String getName() {
        return name;
    }

    public int getTicksLeft() {
        return ticksLeft;
    }

    public int getTickTime() {
        return tickTime;
    }
}

