/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/25/2017
 */
package Entities.Effects;

import Entities.Entity;
import Entities.EntityPointer;

/**
 * Defines an Effect that damages the effected entity
 * @author nowace
 * @version 2017.12.25
 */
public class DamageEffect extends Effect {
    private int amount;

    /**
     * Constructs a damage effect with the given damage amount and tick time
     * @param amount amount of damage to do
     * @param tickTime amount of ticks to do damage for
     */
    public DamageEffect(int amount, int tickTime){
        this.isDone = false;
        this.amount = amount;
        this.tickTime = tickTime;
        this.ticksLeft = tickTime;
        this.name = "Damage";
    }

    /**
     * Effects the given entity and ticks down the ticks left
     * @param entity entity to effect
     */
    @Override
    public void effect(EntityPointer entity) {
        super.effect(entity);
        if(ticksLeft > 0){
            entity.getEntity().setEnergy(entity.getEntity().getEnergy() - amount);
            ticksLeft--;
        }else{
            this.isDone = true;
        }
    }
}
