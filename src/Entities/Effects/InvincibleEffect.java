/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/9/2018
 */
package Entities.Effects;

import Entities.Entity;
import Entities.EntityPointer;

/**
 * Defines an Effect that make the effected entity invincible
 * @author nowace
 * @version 2018.01.09
 */
public class InvincibleEffect extends Effect {

    /**
     * Constructs an invincibility effect with the given tick duration
     * @param duration duration of the effect
     */
    public InvincibleEffect(int duration){
        this.isDone = false;
        this.name = "Invincible";
        this.ticksLeft = duration;
        this.tickTime = duration;
    }


    /**
     * Effects the given entity and ticks down the ticks left
     * @param entity entity to effect
     */
    @Override
    public void effect(EntityPointer entity) {
        super.effect(entity);
        if(ticksLeft > 0){
            ticksLeft--;
        }else{
            this.isDone = true;
        }
    }
}
