/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/8/2018
 */
package Entities.Effects;

import Entities.Entity;
import Entities.EntityPointer;

/**
 * Defines an Effect that rotates the effected entity
 * @author nowace
 * @version 2018.01.08
 */
public class DiscordEffect extends Effect {

    private double rotSpeed;

    /**
     * Constructs a discord effect with the given rotation speed and tick time
     * @param rotSpeed rotation speed
     * @param tickTime tick time
     */
    public DiscordEffect(double rotSpeed, int tickTime){
        this.isDone = false;
        this.rotSpeed = rotSpeed;
        this.tickTime = tickTime;
        this.ticksLeft = tickTime;
        this.name = "Discord";
    }

    /**
     * Effects the given entity and ticks down the ticks left
     * @param entity entity to effect
     */
    @Override
    public void effect(EntityPointer entity) {
        super.effect(entity);
        if(ticksLeft > 0){
            entity.getEntity().setRotation(entity.getEntity().getRotation() + this.rotSpeed);
            ticksLeft--;
        }else{
            this.isDone = true;
        }
    }
}
