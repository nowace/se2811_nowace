package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.FastDecorator;
import Entities.Bees.Decorator.FireDecorator;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Bees.Decorator.SlowDecorator;
import Entities.Effects.DiscordEffect;
import Entities.Effects.HealEffect;
import Entities.Effects.InvincibleEffect;
import Entities.Effects.StunEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import Math.Util;

import java.util.List;

/**
 * Created by boulgerb on 12/19/2017.
 */
public class HealFlower extends Flower{
    private static final int ACTIVATION_COST = 10;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;
    private int healAmount;
    private int tickTime;

    /**
     * Constructs a heal flower with the given position heal amount and tick duration
     * @param position position of the flower
     * @param heal amount to heal
     * @param tickTime tick duration of heal effect
     */
    public HealFlower(Vector position, int heal, int tickTime){
        super(new Texture("Assets/Flowers/flower_heal_charged.png"), position);
        this.energy = 10;
        this.cooldown = 10;
        this.currentTime = this.cooldown;
        this.healAmount = heal;
        this.tickTime = tickTime;
    }

    /**
     * Ticks the flower
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }

    /**
     * Effect to put on other entities
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST){
            currentTime = cooldown;
            other.getEntity().addEffect(new HealEffect(healAmount,tickTime));
            energy = 0;
            if(other.getEntity() instanceof Bee){
                other.setEntity(((Bee)other.getEntity()).removeDecorator(FireDecorator.class));
                other.setEntity(((Bee)other.getEntity()).removeDecorator(SlowDecorator.class));
                other.setEntity(((Bee)other.getEntity()).removeDecorator(FastDecorator.class));
            }
        }
    }

}
