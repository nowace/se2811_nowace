package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.FireDecorator;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Bees.Decorator.SlowDecorator;
import Entities.Effects.InvincibleEffect;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import Math.Util;

import java.util.List;

/**
 * Describes a flower that makes bees that touch it kill other bees they touch
 * Created by nowace
 * @version 2018.1.6
 */
public class FireFlower extends Flower {
    private static final int ACTIVATION_COST = 10;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;
    private int damage;
    private int tickTime;

    /**
     * Constructs a fire flower with the given position and parameters for the decorator to apply
     * @param position
     * @param damage
     * @param tickTime
     */
    public FireFlower(Vector position, int damage, int tickTime) {
        super(new Texture("Assets/Flowers/flower_fire.png"), position);
        this.damage = damage;
        this.tickTime = tickTime;
        this.energy = 10;
        this.cooldown = 10;
        this.currentTime = this.cooldown;
    }

    /**
     * Effects the other by placing a firedecorator on it if it is a bee.
     * Also removes a shield decorator and slow decorator if present
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST && !other.getEntity().isInvincible()){
            currentTime = cooldown;
            if(other.getEntity() instanceof Bee){
                other.setEntity(new FireDecorator((Bee)other.getEntity(),damage,tickTime));
            }
            energy = 0;
        }

        if(other.getEntity() instanceof Bee){
            other.setEntity(((Bee)other.getEntity()).removeDecorator(ShieldDecorator.class));
            other.setEntity(((Bee)other.getEntity()).removeDecorator(SlowDecorator.class));
            other.getEntity().addEffect(new InvincibleEffect(10));

        }
    }

    /**
     * Ticks up the energy randomly if not at activation cost
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }

    @Override
    public EntityPointer checkCollisions(List<EntityPointer> entities) {
        return super.checkCollisions(entities);
    }
}
