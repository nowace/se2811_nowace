package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.FireDecorator;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Bees.Decorator.SlowDecorator;
import Entities.Effects.HealEffect;
import Entities.Effects.InvincibleEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import Math.Util;

import java.util.List;

/**
 * Describes a flower that applies the slow decorator to bees that touch it
 * Created by nowace
 * @version 2018.1.6
 */
public class SlowFlower extends Flower {
    private static final int ACTIVATION_COST = 10;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;
    private int tickTime;
    private double speedFactor;


    /**
     * Constructs a slow flower with the given position and slowdown factor
     * @param position
     * @param speedFactor
     */
    public SlowFlower(Vector position, double speedFactor) {
        super(new Texture("Assets/Flowers/flower_ice.png"), position);
        this.energy = 10;
        this.cooldown = 10;
        this.currentTime = this.cooldown;
        this.speedFactor = speedFactor;
    }

    /**
     * Adds a slowdown effect to the other entity if it is a bee
     * and removes a shield decorator and fire decorator if it is present
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST && !other.getEntity().isInvincible()){
            currentTime = cooldown;
            if(other.getEntity() instanceof Bee){
                other.setEntity(new SlowDecorator((Bee)other.getEntity(),speedFactor));
                other.getEntity().addEffect(new InvincibleEffect(10));

            }
            energy = 0;
        }
        if(other.getEntity() instanceof Bee){
            other.setEntity(((Bee)other.getEntity()).removeDecorator(ShieldDecorator.class));
            other.setEntity(((Bee)other.getEntity()).removeDecorator(FireDecorator.class));
        }
    }

    /**
     * Ticks the flower
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }
}
