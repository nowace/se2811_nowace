package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import Math.Util;

/**
 * Describes a flower that applies the shield decorator to bees that hit it
 * Created by nowace
 * @version 2018.1.6
 */
public class ShieldFlower extends Flower {
    private static final int ACTIVATION_COST = 100;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;

    /**
     * Constructs a shiled flower at the given position
     * @param position
     */
    public ShieldFlower(Vector position) {
        super(new Texture("Assets/Flowers/flower_shield.png"), position);
        this.energy = 100;
        this.cooldown = 100;
        this.currentTime = this.cooldown;
    }

    /**
     * Gives the other entity a shiled decorator if it is a bee
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST){
            currentTime = cooldown;
            if(other.getEntity() instanceof Bee){
                other.setEntity(new ShieldDecorator((Bee)other.getEntity()));
            }
            energy = 0;
        }
    }

    /**
     * Ticks up the energy randomly if not at activation cost
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }
}
