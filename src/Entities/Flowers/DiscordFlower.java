/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/8/2018
 */
package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Effects.DiscordEffect;
import Entities.Effects.InvincibleEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;

import java.util.List;
import Math.Vector;
import Math.Util;

/**
 * Defines a flower that adds the discord effect
 * @author nowace
 * @version 2018.01.08
 */
public class DiscordFlower extends Flower {
    private static final int ACTIVATION_COST = 10;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;
    private double rotSpeed;
    private int tickTime;

    /**
     * Constructs a discord flower with the given position rotation speed and tick duration
     * @param position position of flower
     * @param rotSpeed rotation speed of discord effect
     * @param tickTime tick duration of the discord effect
     */
    public DiscordFlower(Vector position, double rotSpeed, int tickTime) {
        super(new Texture("Assets/Flowers/flower_discord.png"), position);
        this.energy = 10;
        this.cooldown = 10;
        this.currentTime = 10;
        this.rotSpeed = rotSpeed;
        this.tickTime = tickTime;
    }

    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }

    /**
     * Effect to put on other entities
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST && !other.getEntity().isInvincible()){
            currentTime = cooldown;
            other.getEntity().addEffect(new DiscordEffect(rotSpeed,tickTime));
            energy = 0;

        }
        if(other.getEntity() instanceof Bee){
            other.setEntity(((Bee)other.getEntity()).removeDecorator(ShieldDecorator.class));
            other.getEntity().addEffect(new InvincibleEffect(10));

        }
    }
}
