package Entities.Flowers;


import Entities.Bees.Bee;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Effects.DamageEffect;
import Entities.Effects.InvincibleEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Texture;
import Math.Vector;
import Math.Util;

import java.util.List;

/**
 * Defines a flower that adds the damage effect
 * Created by boulgerb on 12/19/2017.
 */
public class DamageFlower extends Flower {
    private static final int ACTIVATION_COST = 10;
    private static final int ENERGY_GAIN_CHANCE = 5;
    private static final int ENERGY_GAIN_AMOUNT = 1;
    private int damageAmount;
    private int tickTime;

    /**
     * Constructs a damage flower at the given position with the given damage and tick duration
     * @param position position of flower
     * @param damage damage of damage effect
     * @param tickTime tick duration of damage effect
     */
    public DamageFlower(Vector position, int damage, int tickTime){
        super(new Texture("Assets/Flowers/flower_poisn.png"), position);
        this.energy = 10;
        this.cooldown = 10;
        this.currentTime = this.cooldown;
        this.damageAmount = damage;
        this.tickTime = tickTime;
    }

    /**
     * Ticks the flower
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(energy != ACTIVATION_COST){
            energy += (Util.RandomInt(0,101) < ENERGY_GAIN_CHANCE)?ENERGY_GAIN_AMOUNT:0;
        }
    }

    /**
     * Effect to put on other entities
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        super.effect(other);
        if(currentTime < 0 && energy == ACTIVATION_COST && !other.getEntity().isInvincible()){
            currentTime = cooldown;
            other.getEntity().addEffect(new DamageEffect(damageAmount,tickTime));
            energy = 0;

        }
        if(other.getEntity() instanceof Bee){
            other.setEntity(((Bee)other.getEntity()).removeDecorator(ShieldDecorator.class));
            other.getEntity().addEffect(new InvincibleEffect(10));

        }
    }
}
