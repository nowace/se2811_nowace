package Entities.Flowers;

import Entities.Bees.Bee;
import Entities.Bees.Decorator.ShieldDecorator;
import Entities.Bees.ZigZagBee;
import Entities.Effects.InvincibleEffect;
import Entities.Effects.StunEffect;
import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Sprite;
import Graphics.Texture;
import Math.BoundingGeometry.BoundingCircle;
import Math.Vector;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.TextField;

import java.util.LinkedList;
import java.util.List;

/**
 * Defines a basic flower
 * Created by boulgerb on 12/19/2017.
 */
public abstract class Flower extends Sprite{
    private static final int STUN_DURATION = 4;
    private static final int INVINCIBILITY_DURATION = 10;
    private static final int BOUNDS_RADIUS_OFFSET = 10;

    protected int cooldown;
    protected int currentTime;

    /**
     * Constructs a flower with the given texture and position
     * @param texture texture of the flower
     * @param position position of the flower
     */
    public Flower( Texture texture, Vector position){
        this.effects = new LinkedList<>();
        this.position = position;
        this.velocity = new Vector(0,0);
        this.rotation = 0;
        this.dead = false;
        this.energy = 1;
        this.texture = texture;
        this.origin = new Vector(this.texture.getImage().getWidth()/2,this.texture.getImage().getHeight()/2);
        bound = new BoundingCircle(position,
                ((texture.getImage().getWidth()>texture.getImage().getHeight())?
                        texture.getImage().getWidth()/2:
                        texture.getImage().getHeight()/2) - BOUNDS_RADIUS_OFFSET);
    }

    public int getCooldown() {
        return cooldown;
    }

    /**
     * Ticks the flower
     * @param timestep timestep of tick
     */
    @Override
    public void tick(double timestep) {
        super.tick(timestep);
        if(currentTime >= 0){
            this.currentTime--;
        }
    }

    /**
     * Effect to put on other entities
     * @param other other entity
     */
    @Override
    public void effect(EntityPointer other) {
        //other.addEffect(new StunEffect(STUN_DURATION,other.getVelocity()));
        //other.addEffect(new InvincibleEffect(INVINCIBILITY_DURATION));
    }

    @Override
    public EntityPointer checkCollisions(List<EntityPointer> entities) {
        return null;
    }
}
