package Entities;

/**
 * Describes a pointer to an entity
 * Created by nowace
 * @version 2018.1.6
 */
public class EntityPointer {
    private Entity entity;

    /**
     * Constructs the pointer with the given entity
     * @param e
     */
    public EntityPointer(Entity e){
        this.entity = e;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }
}
