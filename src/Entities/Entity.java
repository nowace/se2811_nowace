/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Entities;
import Entities.Effects.Effect;
import Math.BoundingGeometry.Bound;
import Math.Vector;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.LinkedList;
import java.util.List;

/**
 * Defines a basic entity
 * @author nowace
 * @version 2017.12.14
 */
public abstract class Entity {
    protected static final int DEBUG_TEXT_ENTRY_OFFSET = 20;
    protected static final int DEBUG_TEXT_X_OFFSET = 40;
    protected static final int DEBUG_TEXT_Y_OFFSET = 40;
    protected static final int MAXIMUM_TIMESTEP_THRESHOLD = 60;

    protected Vector position;
    protected Vector velocity;
    protected Vector origin;
    protected double rotation;
    protected Bound bound;
    protected boolean dead;
    protected int energy;
    protected List<Effect> effects;
    protected boolean invincible;
    protected EntityPointer self;
    protected int debugListStepper;
    protected List<String> debugStrings;

    public Entity(){
        self = new EntityPointer(this);
        debugStrings = new LinkedList<>();
    }

    /**
     * Starts the draw procedure of the entity
     * @param context context to draw to
     */
    public void draw(GraphicsContext context){
        context.save();
        context.translate(position.getX(),position.getY());
        context.rotate(rotation);
    }

    /**
     * Draws the debug information of the entity
     * @param context
     */
    public void drawDebug(GraphicsContext context){
        context.save();
        context.translate(position.getX() + DEBUG_TEXT_X_OFFSET,position.getY() - DEBUG_TEXT_Y_OFFSET);
        context.setFill(Color.WHITE);
        context.fillText("" + this.energy,0,0);
        debugListStepper = 0;
        for(Effect e : effects){
            context.fillText(e.getName() + ": " + e.getTicksLeft() + "/" + e.getTickTime(), 0,DEBUG_TEXT_ENTRY_OFFSET + debugListStepper);
            debugListStepper+=DEBUG_TEXT_ENTRY_OFFSET;
        }
        for(String s : debugStrings){
            context.fillText(s,0,DEBUG_TEXT_ENTRY_OFFSET + debugListStepper);
            debugListStepper+=DEBUG_TEXT_ENTRY_OFFSET;
        }
        debugStrings.clear();
        context.restore();
        getBound().draw(context);
    }

    /**
     * Ticks the entity
     * @param timestep timestep of tick
     */
    public void tick(double timestep){
        if(timestep > MAXIMUM_TIMESTEP_THRESHOLD) return;
        List<Effect> toRemove = new LinkedList<>();
        this.invincible = effects.stream().anyMatch(effect -> effect.getName().equals("Invincible"));
        for(Effect e : effects){
            e.effect(self);
            if(e.isDone()){
                toRemove.add(e);
            }
        }
        effects.removeAll(toRemove);
        position = position.add(velocity.scalar(timestep));
    }

    public abstract EntityPointer checkCollisions(List<EntityPointer> entities);

    public Vector getPosition(){
        return position;
    }

    public void setPosition(Vector position){
        this.position = position;
    }

    public Vector getOrigin(){
        return origin;
    }

    public void setOrigin(Vector origin){
        this.origin = origin;
    }

    public Vector getVelocity(){
        return velocity;
    }

    public void setVelocity(Vector vel){
        this.velocity = vel;
    }

    public double getRotation(){
        return this.rotation;
    }

    public void setRotation(double rotation){
        this.rotation = rotation;
    }

    public Bound getBound(){
        return this.bound;
    }

    public boolean isDead() {
        return dead;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void addEffect(Effect effect){
        if(!effects.contains(effect) && !invincible){
            effects.add(effect);
        }
    }

    public abstract void effect(EntityPointer other);

    public List<Effect> getEffects() {
        return effects;
    }

    public boolean isInvincible() {
        return invincible;
    }

    public void setInvincible(boolean invincible) {
        this.invincible = invincible;
    }

    public EntityPointer getPointer() {
        return self;
    }

    public int getDebugListStepper() {
        return debugListStepper;
    }

    public void setDebugListStepper(int debugListStepper) {
        this.debugListStepper = debugListStepper;
    }

    public void addDebugString(String string){
        debugStrings.add(string);
    }
}