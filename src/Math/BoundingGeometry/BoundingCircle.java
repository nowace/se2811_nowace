/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Math.BoundingGeometry;

import Math.Vector;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * @author nowace
 * @version 2017.12.16
 */
public class BoundingCircle extends Bound {
    private double radius;


    public BoundingCircle(Vector position, double radius){
        this.position = position;
        this.radius = radius;
    }

    @Override
    public boolean intersects(Bound other) {
        if(other instanceof BoundingCircle){
            BoundingCircle circle = (BoundingCircle)other;
            double distance = this.position.distanceSQRT(circle.position);
            if(distance < this.radius+circle.radius){
                return true;
            }else{
                return false;
            }
        }else if(other instanceof BoundingBox){
            BoundingBox box = (BoundingBox)other;

        }
        return false;
    }

    @Override
    public void draw(GraphicsContext context) {
        context.setStroke(Color.RED);
        context.strokeOval(position.getX()-radius,position.getY()-radius,radius*2,radius*2);
    }

    @Override
    public Vector getSize() {
        return new Vector(radius * 2,radius * 2);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}