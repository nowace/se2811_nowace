/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Math.BoundingGeometry;
import Math.Vector;
import javafx.scene.canvas.GraphicsContext;

/**
 * @author nowace
 * @version 2017.12.16
 */
public abstract class Bound {
    protected Vector position;
    public abstract boolean intersects(Bound other);
    public abstract void draw(GraphicsContext context);

    public Vector getPosition(){
        return position;
    }

    public void setPosition(Vector position){
        this.position = position;
    }

    public abstract Vector getSize();
}
