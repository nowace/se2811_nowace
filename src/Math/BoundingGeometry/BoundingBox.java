/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Math.BoundingGeometry;

import javafx.scene.canvas.GraphicsContext;
import Math.Vector;

/**
 * @author nowace
 * @version 2017.12.16
 */
public class BoundingBox extends Bound {
    private Vector size;

    public BoundingBox(Vector position, Vector size){
        this.position = position;
        this.size = size;
    }

    @Override
    public boolean intersects(Bound other) {

        if(other instanceof BoundingBox){
            BoundingBox box = (BoundingBox)other;

            if(this.position.getX() < box.position.getX() + box.size.getX() &&
                    this.position.getX() + this.size.getX() > box.position.getX() &&
                    this.position.getY() < box.position.getY() + box.size.getY() &&
                    this.size.getY() + this.position.getY() > box.position.getY()){
                return true;
            }else{
                return false;
            }
        }else if(other instanceof BoundingCircle){

        }

        return false;
    }

    @Override
    public void draw(GraphicsContext context) {

    }

    public Vector getSize() {
        return size;
    }

    public void setSize(Vector size) {
        this.size = size;
    }
}