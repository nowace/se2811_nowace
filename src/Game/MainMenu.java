/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/25/2017
 */
package Game;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 *  * Controller for a main menu vbox
 * @author nowace
 * @version 2017.12.25
 */
public class MainMenu extends VBox {

    @FXML Button gameButton;
    @FXML Button howToButton;
    @FXML Button creditsButton;
    @FXML Button exitButton;

    /**
     * Loads the fxml and attaches itself as the root and controller
     */
    public MainMenu(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("mainMenu.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            fxmlLoader.load();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        gameButton.setOnAction(event->{
            this.fireEvent(new StateSelectedEvent("game"));
        });

        howToButton.setOnAction(event -> {
            this.fireEvent(new StateSelectedEvent("howTo"));
        });

        creditsButton.setOnAction(event -> {
            this.fireEvent(new StateSelectedEvent("credits"));
        });

        exitButton.setOnAction(event -> {
            this.fireEvent(new StateSelectedEvent("exit"));
        });
    }

    public void setOnStateChanged(EventHandler<StateSelectedEvent> eventHandler){
        this.addEventHandler(StateSelectedEvent.STATE_SELCTED_EVENT, eventHandler);
    }

}
