/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Game;

import Entities.Entity;
import Entities.EntityPointer;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;

import java.util.LinkedList;
import java.util.List;

/**
 * @author nowace
 * @version 2017.12.16
 */
public class UpdateLoop extends AnimationTimer {

    private List<EntityPointer> entities;
    private long previous;
    private double deltaTime;
    private boolean tick;
    private boolean tickBased;
    private double timeStep;

    public static List<EntityPointer> spawnList = new LinkedList<>();


    public UpdateLoop(List<EntityPointer> entities){
        this.entities = entities;
        timeStep = 1;
        tickBased = true;
        tick = false;
        deltaTime = 0;
        previous = 0;
    }

    @Override
    public void handle(long now) {
        deltaTime = (now - previous) * 1e-9;
        previous = now;
        List<EntityPointer> toDelete = new LinkedList<>();

        if(tickBased){
            if(tick){
                for(EntityPointer e : entities){
                    e.getEntity().tick(timeStep/60);
                    e.getEntity().checkCollisions(entities);
                    if(e.getEntity().isDead()){
                        toDelete.add(e);
                    }
                }
                entities.removeAll(toDelete);
                toDelete.clear();
                if(!spawnList.isEmpty()){
                    entities.addAll(spawnList);
                    spawnList.clear();
                }
                tick = false;
            }
        }else{
            for(EntityPointer e : entities){
                e.getEntity().tick(deltaTime);
                e.getEntity().checkCollisions(entities);
                if(e.getEntity().isDead()){
                    toDelete.add(e);
                }
            }
            entities.removeAll(toDelete);
            toDelete.clear();
            if(!spawnList.isEmpty()){
                entities.addAll(spawnList);
                spawnList.clear();
            }
        }
    }

    public void tick(){
        tick = true;
    }

    public double getTimeStep() {
        return timeStep;
    }

    public boolean isTickBased() {
        return tickBased;
    }

    public double getDeltaTime() {
        return deltaTime;
    }

    public void setTickBased(boolean tickBased) {
        this.tickBased = tickBased;
    }

    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }
}
