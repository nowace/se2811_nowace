/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/25/2017
 */
package Game;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * Controller for a credits menu vbox
 * @author nowace
 * @version 2017.12.25
 */
public class CreditsMenu extends VBox {

    @FXML Button backButton;

    /**
     * Loads the fxml and attaches itself as the root and controller
     */
    public CreditsMenu(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("creditsMenu.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try{
            fxmlLoader.load();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        backButton.setOnAction(event->{
            this.fireEvent(new StateSelectedEvent("done"));
        });
    }

    public void setOnStateChanged(EventHandler<StateSelectedEvent> eventHandler){
        this.addEventHandler(StateSelectedEvent.STATE_SELCTED_EVENT, eventHandler);
    }
}
