package Game;/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */

import Audio.Music;
import Entities.Entity;
import Entities.EntityPointer;
import javafx.beans.NamedArg;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nowace
 * @version 2017.12.16
 */
public abstract class Game extends Scene {
    private Canvas canvas;
    protected List<EntityPointer> entities;
    protected DrawLoop drawLoop;
    protected UpdateLoop updateLoop;

    public Game(@NamedArg("root") Parent root, Canvas canvas) {
        super(root);
        this.canvas = canvas;
        this.entities = new ArrayList<>();
        this.drawLoop = new DrawLoop(canvas, entities);
        this.updateLoop = new UpdateLoop(entities);
    }

    public void start(){
        drawLoop.start();
        updateLoop.start();
    }

    public void stop(){
        this.drawLoop.stop();
        this.updateLoop.stop();
        entities.clear();
        init();
    }

    public void setOnStateChanged(EventHandler<StateSelectedEvent> eventHandler){
        this.canvas.addEventHandler(StateSelectedEvent.STATE_SELCTED_EVENT, eventHandler);
    }

    public abstract void init();
}
