/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/25/2017
 */
package Game;

import javafx.beans.NamedArg;
import javafx.event.Event;
import javafx.event.EventType;

/**
 * Event describing when a state change occurs
 * @author nowace
 * @version 2017.12.25
 */
public class StateSelectedEvent extends Event {
    public static EventType<StateSelectedEvent> STATE_SELCTED_EVENT = new EventType<StateSelectedEvent>("STATE_SELECTED_EVENT");
    private String stateName;

    /**
     * Creates a StateSelectedEvent with the given name of state
     * @param stateName name of state
     */
    public StateSelectedEvent(String stateName) {
        super(STATE_SELCTED_EVENT);
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }
}
