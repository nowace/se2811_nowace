package Game;

import Entities.Bees.Bee;
import Entities.Bees.CircleBee;
import Entities.Bees.Decorator.*;
import Entities.Bees.FlowerFinderBee;
import Entities.Bees.ZigZagBee;
import Entities.EntityPointer;
import Entities.Flowers.*;
import Graphics.Background;
import Math.Vector;
import javafx.beans.NamedArg;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;

import java.util.stream.Collectors;

/**
 * Defines a game called FlowerBed
 * Created by boulgerb on 12/19/2017.
 */
public class FlowerBed extends Game {
    public FlowerBed(@NamedArg("root") Parent root, Canvas canvas) {
        super(root, canvas);
        init();
        this.setOnKeyPressed(event -> {
            if(event.getCode().equals(KeyCode.UP) || event.getCode().equals(KeyCode.SPACE)){
                this.updateLoop.tick();
            }else if(event.getCode().equals(KeyCode.D)){
                this.drawLoop.setDebug(!this.drawLoop.getDebug());
            }else if(event.getCode().equals(KeyCode.F)){
                this.updateLoop.setTickBased(!this.updateLoop.isTickBased());
            }else if(event.getCode().equals(KeyCode.ESCAPE)){
                canvas.fireEvent(new StateSelectedEvent("done"));
            }
        });
    }

    @Override
    public void init() {
        //DiscordFlower flower = new DiscordFlower(new Vector(100,100),100,100);
        //entities.add(flower.getPointer());
//
        //HealFlower flower1 = new HealFlower(new Vector(350,100),20,2);
        //entities.add(flower1.getPointer());

        Flower flower2 = new FireFlower(new Vector(700,100),1,5);
        entities.add(flower2.getPointer());

        Flower flower3 = new ShieldFlower(new Vector(100,400));
        entities.add(flower3.getPointer());

        //entities.add(new DamageFlower(new Vector(350,400),20,2).getPointer());

        entities.add(new SlowFlower(new Vector(700,400),0.5).getPointer());

        //Bee bee2 = new ZigZagBee(new Vector(100,100),new Vector(800,600));
        //entities.add(bee2.getPointer());

        Bee bee3 = new FlowerFinderBee(new Vector(600,600),entities.stream().filter(entity -> entity.getEntity() instanceof Flower).collect(Collectors.toList()));
        entities.add(bee3.getPointer());

        Bee bee4 = new FlowerFinderBee(new Vector(600,600),entities.stream().filter(entity -> entity.getEntity() instanceof Flower).collect(Collectors.toList()));
        entities.add(bee4.getPointer());

        Bee bee = new CircleBee(new Vector(400,400),0,100,10);
        entities.add(bee.getPointer());

        Background background = new Background("Assets/background.png");
        this.drawLoop.setBackground(background);

        this.drawLoop.setDebug(true);
    }

    private void printDecorators(Bee bee){
        while(bee instanceof BeeDecorator){
            System.out.println(bee.getClass().getName());
            bee = ((BeeDecorator)bee).getBee();
        }
    }
}
