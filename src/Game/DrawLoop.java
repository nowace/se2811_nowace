/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 12/16/2017
 */
package Game;

import Entities.Entity;
import Entities.EntityPointer;
import Graphics.Background;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.util.List;

/**
 * Defines a drawloop for a game
 * @author nowace
 * @version 2017.12.16
 */
public class DrawLoop extends AnimationTimer {

    private Canvas canvas;
    private List<EntityPointer> entities;
    private Background background;
    private long previous;
    private double deltaTime;
    private boolean debug;

    /**
     * Constructs a drawloop with the given canvas and refrence to entity list
     * @param canvas
     * @param entityList
     */
    public DrawLoop(Canvas canvas, List<EntityPointer> entityList){
        this.entities = entityList;
        this.canvas = canvas;
        previous = 0;
        deltaTime = 0;
        this.background = null;
        debug = false;
    }

    /**
     * Method that gets called each frame
     * @param now current time
     */
    @Override
    public void handle(long now) {
        deltaTime = (now - previous) * 1e-9;
        previous = now;

        //Clear screen
        canvas.getGraphicsContext2D().clearRect(0,0,canvas.getWidth(),canvas.getHeight());
        canvas.getGraphicsContext2D().setFill(Color.WHITE);
        canvas.getGraphicsContext2D().fillRect(0,0,canvas.getWidth(),canvas.getHeight());
        //Set background
        if(background != null){
            background.draw(canvas.getGraphicsContext2D());
        }
        //Draw entities and debug
        entities.forEach(entity -> entity.getEntity().draw(canvas.getGraphicsContext2D()));
        if(debug){
            entities.forEach(entity -> entity.getEntity().drawDebug(canvas.getGraphicsContext2D()));
        }
    }

    public Background getBackground() {
        return background;
    }

    public void setBackground(Background background) {
        this.background = background;
    }

    public void setDebug(boolean debug){
        this.debug = debug;
    }

    public boolean getDebug(){
        return debug;
    }
}
