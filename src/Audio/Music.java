/*
 * se2811-18q2-021-team2
 * Eric Nowac
 * Created 1/5/2018
 */
package Audio;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.net.MalformedURLException;

/**
 * A helper class to simplify playing music
 * @author nowace
 * @version 2018.01.05
 */
public class Music {
    private Media media;
    private MediaPlayer player;

    /**
     * Creates a Music object with the given filepath
     * @param filepath path to music file
     * @throws MalformedURLException
     */
    public Music(String filepath) throws MalformedURLException {
        media = new Media(new File(filepath).toURI().toURL().toString());
        player = new MediaPlayer(media);
    }


    public void start(){
        player.play();
    }

    public void stop(){
        player.stop();
    }

    public void pause(){
        player.pause();
    }

    /**
     * Sets the filepath of the Music
     * @param filepath filepath to the music
     * @throws MalformedURLException
     */
    public void set(String filepath) throws MalformedURLException {
        player.stop();
        media = new Media(new File(filepath).toURI().toURL().toString());
        player = new MediaPlayer(media);
    }

    public void setVolume(double volume){
        player.setVolume(volume);
    }
}
